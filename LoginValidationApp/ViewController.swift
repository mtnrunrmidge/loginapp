//
//  ViewController.swift
//  LoginValidationApp
//
//  Created by Margaret Schroeder on 10/26/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var _submitButton: UIButton!
    @IBOutlet weak var _username: UITextField!
    @IBOutlet weak var _password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let preferences = UserDefaults.standard
        if(preferences.object(forKey: "session") != nil)
        {
            SessionDone()
        }
        else
        {
            SessionToDo()
        }
    }
    @IBAction func SubmitBtn(_ sender: Any) {
        
        if(_submitButton.titleLabel?.text == "Logout")
        {
            let preferences = UserDefaults.standard
            preferences.removeObject(forKey: "session")
            
            SessionToDo()
            return
        }
        let username = _username.text
        let password = _password.text
        
        //check for empty entries
        if(username == "" || password == ""){
            return
        }
        
        DoSubmit(username!, password!)
    }
    
    func DoSubmit(_ usr:String, _ pwd:String){
        let url = URL(string: "https://www.kaleidosblog.com/tutorial/login/api/login")
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        let paramToSend = "username=" + usr + "password=" + pwd
        request.httpBody = paramToSend.data(using: String.Encoding.utf8)
        let task = session.dataTask(with: request as URLRequest, completionHandler:{ (data, response, error) in
            guard let _:Data = data else
            {
                return
            }
            let json: Any?
            
            do
            {
                json = try JSONSerialization.jsonObject(with: data!, options: [])
            }
            catch
            {
                return
            }
            
            guard let server_response = json as? NSDictionary else
            {
                return
            }
            if let data_block = server_response["data"] as? NSDictionary
            {
                if let session_data = data_block["session"] as? String
                {
                    let preferences = UserDefaults.standard
                    preferences.set(session_data, forKey: "session")
                    
                    DispatchQueue.main.async(execute:self.SessionDone)
                }
            }
        })
        task.resume()
    }
    func SessionToDo()
    {
        _username.isEnabled = true
        _password.isEnabled = true
        _submitButton.setTitle("Login", for: .normal)
    }
    
    func SessionDone(){
        _username.isEnabled = false
        _password.isEnabled = false
        _submitButton.setTitle("Logout", for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

